import pandas as pd
from pythainlp import word_tokenize
import json

def levenshteinDistance(s1, s2):
    if len(s1) > len(s2):
        s1, s2 = s2, s1

    distances = range(len(s1) + 1)
    for i2, c2 in enumerate(s2):
        distances_ = [i2+1]
        for i1, c1 in enumerate(s1):
            if c1 == c2:
                distances_.append(distances[i1])
            else:
                distances_.append(1 + min((distances[i1], distances[i1 + 1], distances_[-1])))
        distances = distances_
    return distances[-1]

wordIn=input('text:')
point=[]
df2=[]
with open('countries.json') as json_data:
	for i1 in json_data:
		df2['text']=i1
  	
		pointword=0.0
  	
		for i2 in word_tokenize(wordIn):
			score=0.0
			for i3 in word_tokenize(i1):
				result = levenshteinDistance(i3, i2)
				bigger = max(len(i3), len(i2))
				result = ((bigger - result) / bigger) * 100
				score+=result
			pointword+=score
		addPer = pointword/len(word_tokenize(i1)+1)
		point.append(addPer)
	df2['point']=point
	chack=df2['point'].max()
	df3=df2[df2['point']==chack]
	df3.drop_duplicates(subset=['event'])
	print(df3['event'])
